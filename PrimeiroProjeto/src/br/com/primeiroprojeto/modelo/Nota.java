package br.com.primeiroprojeto.modelo;

public class Nota
{
	
	private Aluno aluno;
	  private Prova prova;
	  private double nota;
	
	
	
	
  public Aluno getAluno() 
    {
		return aluno;
	}
	public void setAluno(Aluno aluno)
	{
		this.aluno = aluno;
	}
	public Prova getProva() 
	{
		return prova;
	}
	public void setProva(Prova prova) 
	{
		this.prova = prova;
	}
	public double getNota() 
	{
		return nota;
	}
	public void setNota(double nota) 
	{
		this.nota = nota;
	}

}
