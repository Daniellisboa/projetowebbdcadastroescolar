package br.com.primeiroprojeto.modelo;

import java.util.Date;

public class Aluno 
{
   private int id;
   private String nome;
   private String matricula;
   private Date datanascimento;
   private Date datamatricula;
   
   
   public int getId() 
   {
	   return id;
   }
   public void setId(int id) 
   {
	  	this.id = id;
   }
   public String getNome() 
   {
	   return nome;
   }
   public void setNome(String nome) 
   {
	   this.nome = nome;
   }
   public String getMatricula() 
   {
	   return matricula;
   }
   public void setMatricula(String matricula) 
   {
	   this.matricula = matricula;
   }	
   public Date getDatanascimento() 
   {
	   return datanascimento;
   }
   public void setDatanascimento(Date datanascimento) 
   {
	   this.datanascimento = datanascimento;
   }
   public Date getDatamatricula()
   {
	   return datamatricula;
   }
   public void setDatamatricula(Date datamatricula) 
   {
	   this.datamatricula = datamatricula;
   }
   
   
}
