package br.com.primeiroprojeto.modelo;

import java.util.Date;

public class Prova 
{
  private int id;
  private Date datarealizacao;
  private String descricao;
  
  public int getId() 
  {
	return id;
}
public void setId(int id) 
{
	this.id = id;
}
public Date getDatarealizacao() 
{
	return datarealizacao;
}
public void setDatarealizacao(Date datarealizacao) 
{
	this.datarealizacao = datarealizacao;
}
public String getDescricao() 
{
	return descricao;
}
public void setDescricao(String descricao) 
{
	this.descricao = descricao;
}

}
