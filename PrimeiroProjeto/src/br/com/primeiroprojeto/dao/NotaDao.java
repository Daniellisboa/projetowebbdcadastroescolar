package br.com.primeiroprojeto.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import br.com.primeiroprojeto.conexao.Conexao;
import br.com.primeiroprojeto.modelo.Aluno;
import br.com.primeiroprojeto.modelo.Nota;
import br.com.primeiroprojeto.modelo.Prova;

public class NotaDao {
	public int IserirNota(Nota nota, Aluno aluno, Prova prova)
			throws SQLException {
		Conexao conexao = new Conexao();
		Connection conn;
		int resultado = 0;

		String sql = "insert into nota(idAluno,idProva,nota)" + "values('"
				+ aluno.getId() + "','" + prova.getId() + "','"
				+ nota.getNota() + "')";

		conn = conexao.conectar();// retorna a concexao com o banco aberta
		if (conn != null) {
			Statement st = conn.createStatement();
			resultado = st.executeUpdate(sql);
			conexao.desconectar();
		}

		return resultado;

	}
}
