package br.com.primeiroprojeto.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.primeiroprojeto.dao.ProvaDao;
import br.com.primeiroprojeto.modelo.Prova;
import br.com.primeiroprojeto.modelo.Usuario;

@RequestScoped
@ManagedBean
public class ProvaBean {
	
	private Prova prova;
	private ProvaDao provadao;
	private Usuario usuarioLogado;
	
	@PostConstruct
	public void init(){
		prova = new Prova();
		provadao = new ProvaDao();
		
		FacesContext ctx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) ctx.getExternalContext().getSession(false);
		usuarioLogado = (Usuario) session.getAttribute("user");
		
		if(usuarioLogado==null)
		{
			try {
				ctx.getExternalContext().redirect("/PrimeiroProjeto/login.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	    
	}
	
	public void salvarProva(){
		try {if (prova.getDescricao().equalsIgnoreCase("")) {
			 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aten�ao!", "Preencha o campo Descri��o")); 
			 
		 }else if (prova.getDatarealizacao()== null) {
			 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aten�ao!", "Preencha o campo Data de realiza��o"));
			 
	
			 
		 }else {
			 if (provadao.IserirProva(prova)!=0) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!", "Prova salva com Sucesso"));
				 
			 } else {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao Salvar Prova"));
				 
			 }
		 }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Prova getProva() {
		return prova;
	}

	public void setProva(Prova prova) {
		this.prova = prova;
	}

	
	
	
	
}
