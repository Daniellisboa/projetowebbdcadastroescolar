package br.com.primeiroprojeto.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import br.com.primeiroprojeto.dao.AlunoDao;
import br.com.primeiroprojeto.modelo.Aluno;
import br.com.primeiroprojeto.modelo.Usuario;

@RequestScoped 
@ManagedBean
public class AlunoBean {
	
	private Aluno aluno;
	private AlunoDao alunodao;
	private Usuario usuarioLogado;
	
	@PostConstruct
	public void init(){
		aluno = new Aluno();
		alunodao = new AlunoDao();
		FacesContext ctx = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) ctx.getExternalContext().getSession(false);
		usuarioLogado = (Usuario) session.getAttribute("user");
		
		if(usuarioLogado==null)
		{
			try {
				ctx.getExternalContext().redirect("/PrimeiroProjeto/login.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	    
	}
	
	public void salvarAluno() {
		try {
			 if (aluno.getNome().equalsIgnoreCase("")) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aten�ao!", "Preencha o campo Nome")); 
				 
			 }else if (aluno.getMatricula().equalsIgnoreCase("")) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aten�ao!", "Preencha o campo Matricula"));
				 
			 }else if (aluno.getDatamatricula()== null) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aten�ao!", "Preencha o campo Data Matricula"));
				 
			 }else if (aluno.getDatanascimento()== null) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aten�ao!", "Preencha o campo Data Nascimento"));
				 
			 }else {
				 if (alunodao.IserirAluno(aluno) !=0) {
					 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!", "Aluno salvo com Sucesso"));
					 
				 } else {
					 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro!", "Erro ao Salvar aluno"));
					 
				 }
			 }
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	
	
	
	
}
